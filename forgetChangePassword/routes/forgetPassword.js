var express = require('express');
var router = express.Router();
var func=require('./commonFunction');
var sendResponse=require('./sendResponse');
var md5 = require('MD5');
var request=require('request');
var async=require('async');

/* GET users listing. */



router.post('/forgetpassword',function(req,res){

  //console.log(hostName);
    var email=req.body.email;
    var manValues=[email];

    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        }
    ],function(result) {
            userId=result.id;
         func.generateOneTimePassword(res,id,function(callbackPassword) {

                  var to = email;
                  var msg = "change the Password";
                  sub = "<h2> Change the password </h2>";
                  sub += "<h5> Hello form the testing app </h5>";
                  sub += "http://localhost:3000/changePassword?link="+callbackPassword;
                  func.sendMail(res, to, sub, msg, function (resultMail) {
                      if (resultMail == 1) {
                          console.log("succesffully send the mail");
                          sendResponse.successStatusMsg(constant.responseMessage.SEND_DATA, res);
                      }
                      else {
                          console.log("this email is wrong");
                          sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_EMAIL, res);
                      }
                  });

         });
    });
});

router.post('/changepassword',function(req,res){
    var oneTimePassword=req.body.link_password;
    var oldPassword=req.body.old_password;
    var newPassword=req.body.new_password;

    var manValues=[oldPassword,newPassword,oneTimePassword];
    func.checkBlank(res,manValues,function(callback){
        if(callback==null){
            console.log("hello");
            checkPasswordAvailable(req,res,oldPassword,newPassword,oneTimePassword);
        }
        else{
            sendResponse.somethingWentWrongError(res);
        }
    });

});


function checkEmailAvailability(res, email, callback){

        var sql='select `user_id` from `userlogin` where `email`=? limit 1';
        var values=[email];
        //console.log("email check");
        dbConnection.Query(res,sql,values,function(userResponse){
           //  accessToken=userResponse[0].access_token;
            console.log(userResponse);

           // console.log(userResponse.length+"jddsd"+accessToken+"  "+id);
            if(userResponse.length){
                id=userResponse[0].user_id;
                  data={
                      "id":id,
                      "email":email
                  };
               return  callback(data);
            }
            else{
                console.log("wrong email");
                sendResponse.sendErrorMessage(constant.responseMessage.INVALID_EMAIL,res);
                }
            });
        }

function checkPasswordAvailable(req,res,oldpassword,newpassword,oneTimePassword){

      func.checkOneTimePassword(res,oneTimePassword,function(resultUser) {
          userId = resultUser[0].user_id;
          console.log(userId);

          sql = 'select `password`,`one_time_password` from `userlogin` where `user_id`=? limit 1';

          dbConnection.Query(res, sql, [userId], function (resultPassword) {
              console.log("resultpassword"+resultPassword);
              if (resultPassword.length == 0) {
                  console.log("does not return ay row into database with any access token");
                  sendResponse.somethingWentWrongError(res);
              }
              else {
                      var encryptPassword = md5(oldpassword);
                      var password1 = resultPassword[0].password;
                      console.log(encryptPassword == password1);
                      if (encryptPassword == password1) {
                          var newPassword = md5(newpassword);
                          sql = 'update `userlogin` set `password`=?,`one_time_password`=? where `user_id`=? limit 1';
                          dbConnection.Query(res, sql, [newPassword, " ", userId], function (resultUpdatePassword) {
                              if (resultUpdatePassword.length == 0) {
                                  sendResponse.somethingWentWrongError(res);
                              }
                              else {
                                  sendResponse.successStatusMsg(constant.responseMessage.CHANGE_PASSWORD, res);
                              }
                          });
                      }
                      else {
                          console.log("Invaild password");
                          sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_PASSWORD, res);

                      }
                  }

          });
      });
}
module.exports = router;
