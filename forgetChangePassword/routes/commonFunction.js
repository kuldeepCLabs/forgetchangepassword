/**
 * Created by GARG on 3/1/2015.
 */
var sendResponse=require('./sendResponse');
var autht=config.get('emailSettings');
var nodemailer = require('nodemailer');
var passwordgenerator=require('password-generator');
var md5=require('MD5');
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == ''||arr[i] == undefined||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

exports.checkOneTimePassword = function(res,oneTimePass, callback)
{
    var sql = "SELECT `user_id` FROM `userlogin` WHERE `one_time_password`=? LIMIT 1";
    dbConnection.Query(res,sql, [oneTimePass], function(result) {
           console.log(result.length);
        if (result.length > 0) {
            return callback(result);
        } else {
            sendResponse.sendErrorMessage(constant.responseMessage.EXPIRE_SESSION,res);
        }
    });
};

exports.sendMail=function(res,to,sub,msg,callback) {


     var smtpTransport = nodemailer.createTransport("Direct", {debug: true}

     );

    var mailOptions = {
        from: autht.emailFrom,
        to:to,
        subject: msg,
        html: sub
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            sendResponse.somethingWentWrongError(res);
        } else {
              return callback(1);
        }
    });

}

exports.generateOneTimePassword=function(res,id,callback){
    var oneTimePassword=passwordgenerator(10,false);
    var encryptPassword=md5(oneTimePassword);

    console.log("one time password"+oneTimePassword);
    sql='update `userlogin` set `one_time_password`=? where user_id=?';

   dbConnection.Query(res,sql,[encryptPassword,id],function(result){
       if(result.length==0){
           sendResponse.somethingWentWrongError(res);
       }
       else{
           return callback(encryptPassword);
       }
   });
}